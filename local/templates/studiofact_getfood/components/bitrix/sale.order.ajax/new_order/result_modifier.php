<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
use Bitrix\Highloadblock as HL,
    Bitrix\Main\Localization\Loc;
/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);
Loc::loadMessages(__FILE__);
$products = $props = $prop_codes = array();
$hl_values = $hl_tables = $hl_files = array();
foreach($arResult["JS_DATA"]["GRID"]["ROWS"] as $key => $row){
    $products[$key] = $row["data"]["PRODUCT_ID"];
}
foreach($arResult["JS_DATA"]["GRID"]["ROWS"] as $key => $row){
    if(isset($row['data']) && isset($row['data']['PROPS']) && !empty($row['data']['PROPS'])){
        foreach ($row['data']['PROPS'] as $k => $prop){
            $prop_codes[$prop["CODE"]] = $prop["CODE"];
        }
    }
}
if(!empty($products) && !empty($prop_codes)){
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
    $arFilter = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $products);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        foreach($arProps as $key => $p){
            if(array_key_exists($key, $prop_codes) && $p["USER_TYPE"] == "directory"){
                $hl_tables[] = $p["USER_TYPE_SETTINGS"]["TABLE_NAME"];
                $hl_values[] = $p["VALUE"];
                $props[$arFields["ID"]][$p["CODE"]][$p["VALUE"]] = "";
            }
        }
    }
    $hl_tables = array_unique($hl_tables);
    if(!empty($hl_tables)){
        foreach($hl_tables as $HLiB_name){
            $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>$HLiB_name)))->fetch();
            $arR = array();
            if (!empty($hlblock)){
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entity_data_class = $entity->getDataClass();
                $rsData = $entity_data_class::getList(array(
                    "select" => array("*"),
                    "order" => array("ID" => "ASC"),
                    "filter" => array("UF_XML_ID" => $hl_values),
                ));
                while($arData = $rsData->Fetch()){
                    if(isset($arData["UF_FILE"])){
                        $hl_files[$arData["UF_XML_ID"]] = CFile::GetPath($arData["UF_FILE"]);
                    }
                }
            }
        }
        if(!empty($hl_files)){
            foreach ($props as $good_id => $prop){
                foreach ($prop as $code => $p) {
                    foreach ($p as $value => $p2) {
                        $props[$good_id][$code][$value] = $hl_files[$value];
                    }
                }
            }
        }
        foreach($arResult["JS_DATA"]["GRID"]["ROWS"] as $key => $row){
            foreach($row["data"]["PROPS"] as $prop_num => $prop){
                if(isset($props[$row['data']['PRODUCT_ID']][$prop["CODE"]]) && !empty($props[$row['data']['PRODUCT_ID']][$prop["CODE"]])){
                    $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["PROPS"][$prop_num]["FILE"] = array_values($props[$row['data']['PRODUCT_ID']][$prop["CODE"]]);
                }
            }
        }
    }
}
//��������
$arResult['DATES'] = array();
//date("l", mktime(0, 0, 0, 7, 1, 2000))

$objDateTime_cur = new \Bitrix\Main\Type\DateTime(date("H:i"), "H:i");
//\Cbit\Pres\Helpers\Debug::pre($objDateTime_cur->getTimestamp());

$objDateTime = new \Bitrix\Main\Type\DateTime("17:00", "H:i");
//\Cbit\Pres\Helpers\Debug::pre($objDateTime->getTimestamp());
$a = 2;
if( $objDateTime_cur->getTimestamp() <= $objDateTime->getTimestamp()) {
    $a = 1;
}
$arResult['KEY'] = $a;
/*for ( $i = $a;$i <= 3; $i++ ) {
    $arResult['DATES'][$i]['VALUE'] = \Bitrix\Main\Type\DateTime::createFromTimestamp(strtotime("+".$i." days"))->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));
    $arResult['DATES'][$i]['TEXT'] = Loc::getMessage('DATE_'.$i);
    $arResult['DATES'][$i]['INPUT'] = false;
}*/
for ( $i = $a;$i <= 3; $i++ ) {
    $arr['VALUE'] = \Bitrix\Main\Type\DateTime::createFromTimestamp(strtotime("+".$i." days"))->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));
    $arr['TEXT'] = Loc::getMessage('DATE_'.$i);
    $arr['INPUT'] = false;
    $arResult['DATES'][] = $arr;
}
/*$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$order = false;
if($request->isAjaxRequest() ) {
    $order = $request->getQuery("order");
    \Cbit\Pres\Helpers\Debug::pre($order);
    die();
}*/

//\Cbit\Pres\Helpers\Debug::pre($order["DATE"]);
$arResult['DATES_COUNT'] = count($arResult['DATES']);
$arResult['DATES'][$arResult['DATES_COUNT']-1]['VALUE'] = '';
$arResult['DATES'][$arResult['DATES_COUNT']-1]['INPUT'] = true;


?>