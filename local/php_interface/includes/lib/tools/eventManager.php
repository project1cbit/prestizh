<?
/**
 * @Author Evgeniy.Taburyanskiy@gmail.com
 */

namespace Cbit\Pres\Tools;

use Bitrix\Main\Loader;


/**
 *  Класс обеспечивает подписку на события модулей
 * Class EventManager
 * @package Cbit\Lurv\Tools
 */
class EventManager
{
	protected static $isConfigurationLoaded = false;

	/**
	 * @var \Bitrix\Main\EventManager
	 */
	protected static $eventManager = null;

	/**
	 * Массив обработчиков событий
	 * Модуль -> Событие -> Колбеки
	 * *
	 * @var array
	 */

	protected static $handlers = [
			'iblock'        => [
					/*'OnBeforeIBlockElementAdd'    => [
							['Cbit\Pres\Handlers\Iblock', 'setDetailTextFromName'],
					],*/
			],
			'main'          => [
			],
			'socialnetwork' => [],
			'crm'           => [],
			'forum'         => [],
			'cbit.Pres'     => []
	];

	/**
	 *
	 */
	static function init()
	{

		if (static::$isConfigurationLoaded === true) {
			return true;
		}

		static::$eventManager = \Bitrix\Main\EventManager::getInstance();
		static::addEventHandlers();
	}

	/**
	 *
	 */
	protected static function addEventHandlers()
	{
		foreach (static::$handlers as $sModuleName => $arEvents) {
			foreach ($arEvents as $sEventName => $arHandlers) {
				foreach ($arHandlers as $mxCallback) {
					if (is_callable($mxCallback, true)) {
						static::$eventManager->addEventHandler(
								$sModuleName,
								$sEventName,
								$mxCallback,
								$includeFile = false,
								$sort = 10
						);
					}
				}
			}
			static::$isConfigurationLoaded = true;
		}
	}
}
