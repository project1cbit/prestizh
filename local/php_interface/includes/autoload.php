<?php
/**
 * @Author Evgeniy.Taburyanskiy@gmail.com
 */

namespace Cbit\Pres;

use
		\Bitrix\Main\Application,
		\Bitrix\Main\Loader,
		\Bitrix\Main\Localization\Loc,
		\Bitrix\Main\ModuleManager;


/**
 * Регистрируем автолоад классов проекта.
 */
/** @var  $arAgents */
$arAgents = [];

/** @var  $arHelpers */
$arHelpers = [
		'\\Cbit\\Pres\\Helpers\\Debug'   => '/local/php_interface/includes/lib/helpers/debug.php'
];
/** @var  $arHandlers */
$arHandlers = [

];

/** @var  $arTools */
$arTools = [
		'\\Cbit\\Pres\\Tools\\EventManager' => '/local/php_interface/includes/lib/tools/eventManager.php',
];

/** @var  $arOrms */
$arOrms = [];

/** @var  $arOrmHandlers */
$arOrmHandlers = [];

/** @var  $arMain */
$arMain = [];


$arClasses = array_merge($arAgents, $arHelpers, $arHandlers, $arTools, $arOrms, $arOrmHandlers, $arMain);
Loader::registerAutoLoadClasses(null, $arClasses);

/**
 * Инициализируем Константы проекта
 */
if (file_exists(__DIR__.'/lib/constants.php')) {
	require_once(__DIR__.'/lib/constants.php');
}

/**
 * Подключаем необходимые  модули
 */
$arDependencies = [
		'iblock',
		'catalog',
		'sale',
];
foreach ($arDependencies as $sDependency) {
	Loader::includeModule($sDependency);
}


/**
 * Подключаем Вендорные библиотеки.
 */
if (file_exists(__DIR__.'/lib/tools/vendors/autoload.php')) {
	require_once(__DIR__.'/lib/tools/vendors/autoload.php');
}


Tools\EventManager::init();


