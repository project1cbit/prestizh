<?php
/*
* =====================================================
*       Company developer: FirstBit.
*       Developer: Evgeniy Taburyanskiy
*       Site: http://
*       E-mail: Evgeniy Taburyanskiy@gmail.com
*       Copyright (c) 2014-2018 FirstBit
* =====================================================
*        - init.php
*       Date: 16.05.2018  Time: 11:16
* =====================================================
*/

// Подключаем Библиотеки и инструменты  проекта
if (file_exists(__DIR__."/includes/autoload.php")){
	require_once (__DIR__."/includes/autoload.php");
}